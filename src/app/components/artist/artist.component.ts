import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ArtistService } from 'src/app/services/artist.service';
import { ToastrNotificationService } from 'src/app/services/toastr-notification.service';
import { ProductFilter } from 'src/app/models/product.filter';
import { ProductService } from 'src/app/services/products.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'artist',
  templateUrl: './artist.component.html',
  styleUrls: ['./artist.component.css']
})
export class ArtistComponent implements OnInit {

  artistId;
  artistInfo;
  isValidAdmin;

  constructor(private route: ActivatedRoute, public router: Router,
              public artistService: ArtistService,
              public productService: ProductService,
              public userService: UserService,
              private toasterService: ToastrNotificationService) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.artistId = params.id;
    });
    this.isValidAdmin = this.userService.isvalidAdmin();
    this.artistService.getArtistInfo(this.artistId, true).subscribe((resp) => {
      this.artistInfo = resp;
      this.productService.setProductFilter(new ProductFilter(null, this.artistId, 1, 4));
    }, (error) => {
      this.toasterService.showError('Artist information not found');
      this.router.navigate(['/home']);
    });
  }

  editArtist() {
    this.router.navigate(['/admin/artist/add/' + this.artistInfo.artistId]);
  }

}
