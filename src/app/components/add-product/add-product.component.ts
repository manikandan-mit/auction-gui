import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { ArtistService } from 'src/app/services/artist.service';
import { NgbTypeahead } from '@ng-bootstrap/ng-bootstrap';
import { Subject, Observable, merge } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, map } from 'rxjs/operators';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { textEditorConfig } from 'src/app/models/text-editor.config';
import { NgbTimepickerConfig } from '@ng-bootstrap/ng-bootstrap';
import { CategoryService } from 'src/app/services/category.service';
import { faPlusCircle, faTimes } from '@fortawesome/free-solid-svg-icons';
import { ProductService } from 'src/app/services/products.service';
import { ToastrNotificationService } from 'src/app/services/toastr-notification.service';

@Component({
  selector: 'add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css'],
  providers: [NgbTimepickerConfig]
})
export class AddProductComponent implements OnInit {

  productForm: FormGroup;
  artistList = [];
  subCategoryList = [];
  @ViewChild('instance', {static: true}) instance: NgbTypeahead;
  focus$ = new Subject<string>();
  click$ = new Subject<string>();
  editorConfig: AngularEditorConfig = textEditorConfig;
  allowedFileExtensions = ['.jpg', '.jpeg', '.png'];
  fileUploadError = null;
  fileToUpload: File = null;
  numOfAddInfoColumns = 1;
  addIcon = faPlusCircle;
  removeIcon = faTimes;

  formatter = (artist) => artist.artistName;

  constructor(private config: NgbTimepickerConfig,
              private router: Router,
              private formBuilder: FormBuilder,
              private artistService: ArtistService,
              private categoryService: CategoryService,
              private productService: ProductService,
              private toasterService: ToastrNotificationService) {
                this.config.seconds = false;
                this.config.spinners = false;
              }

  ngOnInit(): void {
    this.productForm = new FormGroup({
      productName: new FormControl('', Validators.required),
      artist: new FormControl('', Validators.required),
      productDescription: new FormControl('', Validators.required),
      auctionDate: new FormControl('', Validators.required),
      auctionTime: new FormControl({hour: 18, minute: 0, second: 0}, Validators.required),
      estimatedMin: new FormControl('', Validators.required),
      estimatedMax: new FormControl('', Validators.required),
      category: new FormControl('', Validators.required),
      additionalInfo: new FormArray([])
    });
    this.addNewAddInfoRow();
    this.artistService.listAllActiveArtist().subscribe((resp: any) => {
      this.artistList = resp;
    }, (error) => {
      this.artistList = [];
    });

    this.categoryService.listSubCategory().subscribe((resp: any) => {
      this.subCategoryList = resp;
    }, (error) => {
      this.subCategoryList = [];
    });
  }

  getTodayForDatePicker() {
    const dt = new Date();
    return {
      year: dt.getFullYear(),
      month: dt.getMonth() + 1,
      day: dt.getDate()
    };
  }
  get f() {
    return this.productForm.controls;
  }

  get a() {
    return this.f.additionalInfo as FormArray;
  }

  addNewAddInfoRow() {
    this.numOfAddInfoColumns = this.numOfAddInfoColumns + 1;
    this.a.push(this.formBuilder.group({
      key: ['', Validators.required],
      value: ['', Validators.required]
    }));
  }

  removeRow(i) {
    this.a.removeAt(i);
  }

  addNewProduct() {
    const req = {
      productName: this.f.productName.value,
      productDescription: this.f.productDescription.value,
      auctionDate: this.constructTimeStamp(),
      estimatedMin: this.f.estimatedMin.value,
      estimatedMax: this.f.estimatedMax.value,
      artistId: this.f.artist.value.artistId,
      subCategoryId: this.f.category.value,
      additionalInfo: this.a.value.map(info => info.key + '=' + info.value).join('|')
    };

    const formData: FormData = new FormData();
    formData.append('file', this.fileToUpload, this.fileToUpload.name);
    formData.append('productInfo', new Blob([JSON.stringify(req)], {type: 'application/json'}));
    this.productService.addNewProduct(formData).subscribe((resp) => {
      this.toasterService.showSuccess('Product added successfully!');
      this.router.navigate(['/admin/products']);
    }, (error) => {
      this.toasterService.showError('Something went wrong. Please try later');
    });
  }

  constructAdditionalInfo() {
    return this.a.value.map(info => info.key + '=' + info.value).join('|');
  }

  constructTimeStamp(): Date {
    const dt = new Date();
    dt.setFullYear(this.productForm.value.auctionDate.year);
    dt.setMonth(this.productForm.value.auctionDate.month - 1);
    dt.setDate(this.productForm.value.auctionDate.day);
    dt.setHours(this.productForm.value.auctionTime.hour);
    dt.setMinutes(this.productForm.value.auctionTime.minute);
    dt.setSeconds(0);
    return dt;
  }

  backToProductMgmt() {
    this.router.navigate(['/admin/products']);
  }

  search = (text$: Observable<string>) => {
    const debouncedText$ = text$.pipe(debounceTime(200), distinctUntilChanged());
    const clicksWithClosedPopup$ = this.click$.pipe(filter(() => !this.instance.isPopupOpen()));
    const inputFocus$ = this.focus$;

    return merge(debouncedText$, inputFocus$, clicksWithClosedPopup$).pipe(
      map(term => (term === '' ? this.artistList
        : this.artistList.filter(artist => artist.artistName.toLowerCase().indexOf(term.toLowerCase()) > -1)).slice(0, 10))
    );
  }

  handleFileInput(files: FileList) {
    this.fileUploadError = null;
    this.fileToUpload = null;
    const file = files.item(0);
    const fileExt = file.name.substr(file.name.lastIndexOf('.'));

    if (!this.allowedFileExtensions.includes(fileExt)){
      this.fileUploadError = 'Only .jpg, .jpeg, .png files are allowed';
      return;
    }
    if (file.size > 5242880) {
      this.fileUploadError = 'File size must be below 5MB';
      return;
    }
    this.fileToUpload = files.item(0);
  }
}
