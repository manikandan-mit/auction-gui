import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { textEditorConfig } from 'src/app/models/text-editor.config';
import { ToastrNotificationService } from 'src/app/services/toastr-notification.service';
import { ArtistService } from 'src/app/services/artist.service';
import { Router, ActivatedRoute } from '@angular/router';
import { faTrashAlt } from '@fortawesome/free-solid-svg-icons';
@Component({
  selector: 'add-artist',
  templateUrl: './add-artist.component.html',
  styleUrls: ['./add-artist.component.css']
})
export class AddArtistComponent implements OnInit {

  artistForm: FormGroup;
  artistId;
  editorConfig: AngularEditorConfig = textEditorConfig;
  allowedFileExtensions = ['.jpg', '.jpeg', '.png'];
  fileUploadError = null;
  fileToUpload: File = null;
  artistInfo;
  deletedImgId;
  deleteIcon = faTrashAlt;
  constructor(private toasterService: ToastrNotificationService,
              private artistService: ArtistService,
              private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit(): void {
    this.artistForm = new FormGroup({
      artistId: new FormControl(''),
      artistName: new FormControl('', Validators.required),
      artistDesc: new FormControl('', Validators.required)
    });
    this.route.params.subscribe(params => {
      this.artistId = params.id;
      if (this.artistId) {
        this.artistService.getArtistInfo(this.artistId, false).subscribe((resp: any) => {
          this.artistInfo = resp;
          this.artistForm = new FormGroup({
            artistId: new FormControl(resp.artistId),
            artistName: new FormControl(resp.artistName, Validators.required),
            artistDesc: new FormControl(resp.artistDesc, Validators.required)
          });
        });
      }
    });
  }

  addNewArtist() {

    if (this.artistInfo) {
      this.artistInfo.artistName = this.artistForm.controls.artistName.value;
      this.artistInfo.artistDesc = this.artistForm.controls.artistDesc.value;
      this.saveArtistInfo(this.artistInfo);
    }

    if (this.fileToUpload != null) {
      const formData: FormData = new FormData();
      formData.append('file', this.fileToUpload, this.fileToUpload.name);
      formData.append('artistInfo', new Blob([JSON.stringify(this.artistForm.value)], {type: 'application/json'}));
      this.artistService.saveArtistWithImage(formData, this.deletedImgId).subscribe((resp) => {
        this.toasterService.showSuccess('Artist information saved successfully');
        this.router.navigate(['/admin/artist']);
      }, (error) => {
        this.toasterService.showError('Something went wrong. Please try later');
      });
    } else {
      this.saveArtistInfo(this.artistForm.value);
    }

  }

  saveArtistInfo(obj) {
    this.artistService.saveArtistInfo(obj, this.deletedImgId).subscribe((resp) => {
      this.toasterService.showSuccess('Artist information saved successfully');
      this.router.navigate(['/admin/artist']);
    }, (error) => {
      this.toasterService.showError('Something went wrong. Please try later');
    });
  }

  handleFileInput(files: FileList) {
    this.fileUploadError = null;
    this.fileToUpload = null;
    const file = files.item(0);
    const fileExt = file.name.substr(file.name.lastIndexOf('.'));

    if (!this.allowedFileExtensions.includes(fileExt)){
      this.fileUploadError = 'Only .jpg, .jpeg, .png files are allowed';
      return;
    }
    if (file.size > 5242880) {
      this.fileUploadError = 'File size must be below 5MB';
      return;
    }
    this.artistInfo = null;
    this.fileToUpload = files.item(0);
  }

  clearUploadImage() {
    this.fileToUpload = null;
  }

  removeImageFromInfo() {
    this.fileToUpload = null;
    this.deletedImgId = this.artistInfo.artistProfileUrl;
    this.artistInfo.artistProfileUrl = null;
    this.artistInfo.artistPublicId = null;
  }

  backToArtistMgmt() {
    this.router.navigate(['/admin/artist']);
  }
}
