import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { faStepBackward, faStepForward, faBackward, faForward } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.css']
})
export class PaginationComponent implements OnInit, OnChanges {
  @Input() pageNumber;
  @Input() totalElements;
  @Input() elementsPerPage;
  @Input() rotateOnBorder = false;
  @Input() showItemCount = true;
  @Output() pageChange = new EventEmitter();
  pages: number[];
  totalPages: number;
  prevArrowIcon = faStepBackward;
  nextArrowIcon = faStepForward;
  firstPageIcon = faBackward;
  lastPageIcon = faForward;
  maxPagesToDisplay = 3;
  startNumber: number;
  endNumber: number;
  constructor() { }

  ngOnInit(): void {
    this.initialisePages();
  }

  ngOnChanges() {
    this.initialisePages();
  }

  initialisePages() {
    this.pages = [];
    this.totalPages = Math.ceil(this.totalElements / this.elementsPerPage);
    if (this.totalPages <= this.maxPagesToDisplay) {
      for (let index = 1; index <= this.totalPages; index++) {
        this.pages.push(index);
      }
    } else if (this.totalPages === this.pageNumber) {
      this.pages.push(this.pageNumber - 2);
      this.pages.push(this.pageNumber - 1);
      this.pages.push(this.pageNumber);
    } else if (this.pageNumber === 1) {
      this.pages.push(this.pageNumber);
      this.pages.push(this.pageNumber + 1);
      this.pages.push(this.pageNumber + 2);
    } else {
      this.pages.push(this.pageNumber - 1);
      this.pages.push(this.pageNumber);
      this.pages.push(this.pageNumber + 1);
    }

    this.startNumber = ((this.pageNumber - 1) * this.elementsPerPage) + 1;
    this.endNumber = this.startNumber - 1 + this.elementsPerPage;
    this.endNumber = (this.endNumber > this.totalElements) ? this.totalElements : this.endNumber;
  }

  emitPageChange(pageNumber) {
    if (this.rotateOnBorder) {
      if (pageNumber < 1) {
        pageNumber = this.totalPages;
      }

      if (pageNumber > this.totalPages) {
        pageNumber = 1;
      }
    } else {
    if ((pageNumber < 1) || (pageNumber > this.totalPages)) {
      return;
    }
  }
    this.pageChange.emit(pageNumber);
  }

}
