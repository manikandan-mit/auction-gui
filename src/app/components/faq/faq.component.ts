import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.css']
})
export class FaqComponent implements OnInit {

  selectedCard = 1;

  constructor() { }

  ngOnInit(): void {
  }

  cardClicked(cardId) {
    if (cardId === this.selectedCard) {
      this.selectedCard =  null;
      return;
    }
    this.selectedCard = cardId;
  }

}
