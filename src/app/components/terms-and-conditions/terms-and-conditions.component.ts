import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'terms-and-conditions',
  templateUrl: './terms-and-conditions.component.html',
  styleUrls: ['./terms-and-conditions.component.css']
})
export class TermsAndConditionsComponent implements OnInit {

  selectedCard = 1;

  constructor() { }

  ngOnInit(): void {
  }

  cardClicked(cardId) {
    if (cardId === this.selectedCard) {
      this.selectedCard =  null;
      return;
    }
    this.selectedCard = cardId;
  }

}
