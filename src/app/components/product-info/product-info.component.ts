import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import { ToastrNotificationService } from 'src/app/services/toastr-notification.service';

@Component({
  selector: 'product-info',
  templateUrl: './product-info.component.html',
  styleUrls: ['./product-info.component.css']
})
export class ProductInfoComponent implements OnInit {

  @Input() productDetail;
  timeRemaining;
  auctionTimer;
  constructor(private router: Router,
              private userService: UserService,
              private toasterService: ToastrNotificationService) { }

  ngOnInit(): void {
    if (this.productDetail.timeRemaining) {
      this.timeRemaining = this.productDetail.timeRemaining / 1000;
      this.startTimer();
    }
  }

  startTimer() {
    this.auctionTimer = setInterval(() => {
      this.timeRemaining--;
      if (this.timeRemaining <= 0){
        this.stopTimer();
      }
    }, 1000);
  }

  stopTimer() {
    clearInterval(this.auctionTimer);
  }

  getAdditionalInfo(additionalInfoString: string): any[] {
    const temp: string[] = additionalInfoString.split('|');
    const infoList: any[] = [];
    temp.forEach(t => {
      infoList.push({
        key: t.split('=')[0],
        value: t.split('=')[1]
      });
    });
    return infoList;
  }

  redirectToArtistPage(){
    this.router.navigate(['/artist/' + this.productDetail.artist.artistId]);
  }

  redirectToProductDetails(){
    this.router.navigate(['/products/' + this.productDetail.productId]);
  }

  enterAuction() {
    if (!this.userService.isValidUser()) {
      this.toasterService.showWarning('Please login to enter auction');
    }
  }
}
