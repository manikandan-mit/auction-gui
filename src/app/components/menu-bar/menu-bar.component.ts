import { Component, OnInit } from '@angular/core';
import { MenuBarService } from 'src/app/services/menu-bar.service';
import { faUser } from '@fortawesome/free-regular-svg-icons';
import { ModalService } from 'src/app/services/modal.service';
import { UserService } from 'src/app/services/user.service';
import { SecurityService } from 'src/app/services/security.service';
import { Router } from '@angular/router';
import { ProductFilter } from 'src/app/models/product.filter';
import { ProductService } from 'src/app/services/products.service';

@Component({
  selector: 'menu-bar',
  templateUrl: './menu-bar.component.html',
  styleUrls: ['./menu-bar.component.css']
})
export class MenuBarComponent implements OnInit {

  public categoriesList;
  usericon = faUser;
  showSignupBox = false;
  isValidUser;
  isAdminUser;
  constructor(private menuBarService: MenuBarService,
              private modalService: ModalService,
              private userService: UserService,
              private securityService: SecurityService,
              private productService: ProductService,
              private router: Router) { }

  ngOnInit(): void {
    this.isValidUser = this.userService.isValidUser();
    this.isAdminUser = this.userService.isvalidAdmin();
    this.menuBarService.getCategoriesMenu().subscribe(resp => {
      this.categoriesList = resp;
    }, (error) => {
      this.categoriesList = [];
    });
  }

  openModal(id: string) {
    this.modalService.open(id);
  }

  closeModal(id: string) {
    this.modalService.close(id);
  }

  logout() {
    this.securityService.removeTokenFromLocal();
    window.location.reload();
  }

  routeToProductsPage(categoryId) {
    this.productService.setProductFilter(
      new ProductFilter(categoryId.toString(), null, 1, this.productService.NUMBER_OF_ELEMENTS_PER_PAGE));
    this.router.navigate(['/products'], { queryParams: { id: categoryId }});
  }
}
