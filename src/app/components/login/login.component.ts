import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { SecurityService } from 'src/app/services/security.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  @Output() redirectToSignup = new EventEmitter<boolean>();
  @Output() close = new EventEmitter();
  loginFailed = false;
  loginForm: FormGroup;

  constructor(private securityService: SecurityService) { }

  ngOnInit(): void {
    this.loginForm = new FormGroup({
      username: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required)
    });
  }

  signup() {
    this.loginFailed = false;
    this.loginForm.reset();
    this.redirectToSignup.emit(true);
  }

  login() {
    this.securityService.loginUser(this.loginForm.value).subscribe((response: any) => {
      this.loginFailed = false;
      this.securityService.setTokenInLocal(response.token);
      window.location.reload();
      this.closePopup();
    }, (error) => {
      this.loginFailed = true;
      this.loginForm.controls.password.setValue('');
    });
  }

  closePopup() {
    this.loginFailed = false;
    this.loginForm.reset();
    this.close.emit();
  }
}
