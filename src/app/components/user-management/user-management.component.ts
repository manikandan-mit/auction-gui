import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { ToastrNotificationService } from 'src/app/services/toastr-notification.service';
import { ModalService } from 'src/app/services/modal.service';

@Component({
  selector: 'user-management',
  templateUrl: './user-management.component.html',
  styleUrls: ['./user-management.component.css']
})
export class UserManagementComponent implements OnInit {

  usersList: any;
  selectedUser: any;
  isCallInProgress = false;
  constructor(private userService: UserService, private _toastrNotificationService: ToastrNotificationService,
              private modalService: ModalService) { }

  ngOnInit(): void {
    this.loadUserList();
  }

  loadUserList() {
    this.userService.getAllusers().subscribe((resp) => {
      this.usersList = resp;
    });
  }

  toggleCheckbox(user){
    this.isCallInProgress = true;
    this.userService.updateUserStatus(user.userId, !user.active)
    .subscribe((resp) => {
      this._toastrNotificationService.showSuccess('Status updated successfully');
    }, (error) => {
      this._toastrNotificationService.showError('Status updation failed');
    }).add(() => {
      this.isCallInProgress = false;
      this.loadUserList();
    });
  }

  showUserDetails(user, id) {
    this.selectedUser = user;
    this.modalService.open(id);
  }
}
