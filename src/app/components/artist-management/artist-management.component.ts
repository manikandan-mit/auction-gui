import { Component, OnInit } from '@angular/core';
import { ArtistService } from 'src/app/services/artist.service';
import { ToastrNotificationService } from 'src/app/services/toastr-notification.service';
import { Router } from '@angular/router';
import { faEdit, faEye } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'artist-management',
  templateUrl: './artist-management.component.html',
  styleUrls: ['./artist-management.component.css']
})
export class ArtistManagementComponent implements OnInit {

  artistInfo;
  isCallInProgress = false;
  editIcon = faEdit;
  viewIcon = faEye;
  constructor(private artistService: ArtistService,
              private toasterService: ToastrNotificationService,
              private router: Router) { }

  ngOnInit(): void {
    this.loadArtistInfo();
  }

  loadArtistInfo() {
    this.artistService.getAllArtist().subscribe((resp) => {
      this.artistInfo = resp;
    }, (error) => {
      this.toasterService.showError('Error in reteriving artist info');
    });
  }

  updateArtistStatus(artist: any) {
    this.isCallInProgress = true;
    this.artistService.updateArtistStatus(artist.artistId, !artist.active)
                      .subscribe((resp) => {
                        this.toasterService.showSuccess('Artist info updated successfully');
                      }, (error) => {
                        this.toasterService.showError('Error updating artist status');
                      }).add(() => {
                        this.isCallInProgress = false;
                        this.loadArtistInfo();
                      });
  }

  addNewArtist() {
    this.router.navigate(['/admin/artist/add']);
  }

  editArtist(artistId) {
    this.router.navigate(['/admin/artist/add/' + artistId]);
  }

  viewArtistDetails(artistId) {
    this.router.navigate(['/artist/' + artistId]);
  }
}
