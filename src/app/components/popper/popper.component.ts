import { Component, OnInit, Input, TemplateRef, ViewContainerRef, NgZone, EmbeddedViewRef } from '@angular/core';
import { createPopper, Instance } from '@popperjs/core';

@Component({
  selector: 'app-popper',
  templateUrl: './popper.component.html',
  styleUrls: ['./popper.component.css']
})
export class PopperComponent implements OnInit {

  private _showPopper: boolean;
  @Input() template: TemplateRef<any>;
  @Input() origin: HTMLElement;
  view: EmbeddedViewRef<any>;
  popperRef: Instance;

  constructor(private vcr: ViewContainerRef, private zone: NgZone) { }

  ngOnInit(): void {
    this.origin.classList.add('popper-origin');
  }

  @Input() set showPopper(value: boolean) {
    this._showPopper = value;
    if (this._showPopper) {
      this.view = this.vcr.createEmbeddedView(this.template);
      const tpl = this.view.rootNodes[0];
      tpl.classList.add('popper-modal');

      document.body.appendChild(tpl);
      document.body.addEventListener('click', this.closeOnOutliseClick);
      this.zone.runOutsideAngular(() => {
        this.popperRef = createPopper(this.origin, tpl, {
          placement: 'bottom-start',
          modifiers: [{
            name: 'offset',
            options: {
              offset: [-5, 10]
            }
          }]
        });
      });
    }
  }

  get showPopper() {
    return this._showPopper;
  }

  closeOnOutliseClick = (el: MouseEvent) => {
    let isPopperDiv = false;
    const path: EventTarget[] = el.composedPath();
    path.forEach(element => {
      const name = (element as Element).className;
      if (name && name.indexOf('popper-modal') > -1) {
        isPopperDiv = true;
      }
    });
    const isOrigin = (el.target as Element).className && ((el.target as Element).className.indexOf('popper-origin') > -1);
    if (!isPopperDiv && !isOrigin) {
      this.closePopper();
    }
  }

  closePopper() {
    if (this.popperRef) {
      this.popperRef.destroy();
      this.view.destroy();
      this.view = null;
      this.popperRef = null;
      document.body.removeEventListener('click', this.closeOnOutliseClick);
      }
  }
}
