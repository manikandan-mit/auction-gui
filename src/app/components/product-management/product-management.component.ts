import { Component, OnInit } from '@angular/core';
import { faEye, faEdit } from '@fortawesome/free-solid-svg-icons';
import { ProductService } from 'src/app/services/products.service';
import { ProductFilter } from 'src/app/models/product.filter';
import { ToastrNotificationService } from 'src/app/services/toastr-notification.service';
import { Router } from '@angular/router';
import { NgbTimepickerConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'product-management',
  templateUrl: './product-management.component.html',
  styleUrls: ['./product-management.component.css'],
  providers: [NgbTimepickerConfig, DatePipe]
})
export class ProductManagementComponent implements OnInit {
  pageNumber: number;
  totalNumberOfElements: number;
  elementsPerPage: number;
  productsList = [];
  ELEMENTS_PER_PAGE = 4;
  deactivateProducts = [];
  status = 1;
  viewIcon = faEye;
  editIcon = faEdit;
  editProductId;
  editDate;
  editTime;
  constructor(private config: NgbTimepickerConfig,
              private modalService: NgbModal,
              private productService: ProductService,
              private toasterService: ToastrNotificationService,
              private datePipe: DatePipe,
              private router: Router) {
                config.seconds = false;
                config.spinners = false;
              }

  ngOnInit(): void {
    this.productService.setProductFilter(new ProductFilter(null, null, this.pageNumber, this.ELEMENTS_PER_PAGE));
    this.loadProducts();
  }

  loadProducts() {
    this.deactivateProducts = [];
    this.productService.getAllProducts(true, this.status).subscribe((resp) => {
      this.productsList = resp.products;
      this.pageNumber = resp.pageNumber;
      this.totalNumberOfElements = resp.totalNumberOfElements;
      this.elementsPerPage = resp.productsPerPage;
    }, (error) => {
      this.toasterService.showError('Something went wrong. Please try later');
    });
  }

  pageChange(event) {
    let filter: ProductFilter = this.productService.getProductFilter();
    if (filter) {
      filter.PageNumber = event;
    } else {
      filter = new ProductFilter(null, null, event, this.ELEMENTS_PER_PAGE);
    }
    this.productService.setProductFilter(filter);
    this.loadProducts();
  }

  routeToAddPage() {
    this.router.navigate(['/admin/products/add'])
  }

  checkBox(event, productId) {
    if (event.target.checked) {
      this.deactivateProducts.push(productId);
    } else {
      const index = this.deactivateProducts.indexOf(productId, 0);
      if (index > -1) {
        this.deactivateProducts.splice(index, 1);
      }
    }
  }

  updateStatusOfProducts(status) {
    this.productService.updateProductStatus(this.deactivateProducts, status).subscribe((resp) => {
      this.toasterService.showSuccess('Product status updated successfully');
      this.productService.setProductFilter(new ProductFilter(null, null, 1, this.ELEMENTS_PER_PAGE));
      this.loadProducts();
    }, (error) => {
      this.toasterService.showError('Something went wrong');
    });
  }

  reloadList(selectedStatus) {
    if (this.status !== selectedStatus) {
      this.status = selectedStatus;
      this.productService.setProductFilter(new ProductFilter(null, null, 1, this.ELEMENTS_PER_PAGE));
      this.loadProducts();
    }
  }

  routeToProductDetailsPage(productId) {
    this.router.navigate(['/products/' + productId]);
  }

  getTodayForDatePicker() {
    const dt = new Date();
    return {
      year: dt.getFullYear(),
      month: dt.getMonth() + 1,
      day: dt.getDate()
    };
  }

  openEditPopup(productId, availabledate, content) {
    this.editProductId = productId;
    const dt = new Date(availabledate);
    this.editDate = {
      month: dt.getMonth() + 1,
      year: dt.getFullYear(),
      day: dt.getDate()
    };
    this.editTime = {hour: dt.getHours(), minute: dt.getMinutes()};
    this.modalService.open(content).result.then((ok) => {
      this.productService.updateProductInfo('auctionDate', this.constructTimeStamp(), this.editProductId).subscribe((resp) => {
        this.toasterService.showSuccess('Information updated successfully');
        this.loadProducts();
      }, (error) => {
        this.toasterService.showError('Something went wrong');
      });
    }, (cancel) => {});
  }

  constructTimeStamp(): string {
    const dt = new Date();
    dt.setFullYear(this.editDate.year);
    dt.setMonth(this.editDate.month - 1);
    dt.setDate(this.editDate.day);
    dt.setHours(this.editTime.hour);
    dt.setMinutes(this.editTime.minute);
    dt.setSeconds(0);
    return this.datePipe.transform(dt, 'd-M-y h:mm');
  }
}
