import { Component, OnInit } from '@angular/core';
import { faFacebookSquare } from '@fortawesome/free-brands-svg-icons';
import { faTwitter } from '@fortawesome/free-brands-svg-icons';
import { faPinterestP } from '@fortawesome/free-brands-svg-icons';
import { faInstagram } from '@fortawesome/free-brands-svg-icons';
import { faYoutube } from '@fortawesome/free-brands-svg-icons';

@Component({
  selector: 'footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  fbIcon = faFacebookSquare;
  twitterIcon = faTwitter;
  pinterestIcon = faPinterestP;
  instaIcon = faInstagram;
  youtubeIcon = faYoutube;
  footerBreadCrumbs = [{
    name: 'HOME',
    link: '/home'
  }, {
    name: 'BUY',
    link: '/products'
  }, {
    name: 'SELL',
    link: '/home'
  }, {
    name: 'ABOUT US',
    link: '/about'
  }, {
    name: 'CONTACT US',
    link: '/contact'
  }, {
    name: 'Terms and Conditions',
    link: '/tandc'
  }, {
    name: 'NEWS',
    link: '/home'
  }, {
    name: 'FAQ',
    link: '/faq'
  }];
  // footerBreadCrumbs = ['HOME', 'BUY', 'SELL', 'ABOUT US', 'CONTACT US', 'PRIVACY POLICY', 'NEWS', 'FAQ'];

  constructor() { }

  ngOnInit(): void {
  }
}
