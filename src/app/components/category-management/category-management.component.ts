import { Component, OnInit } from '@angular/core';
import { CategoryService } from 'src/app/services/category.service';
import { ToastrNotificationService } from 'src/app/services/toastr-notification.service';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { ModalService } from 'src/app/services/modal.service';
import { faCaretRight } from '@fortawesome/free-solid-svg-icons';
import { faCaretDown } from '@fortawesome/free-solid-svg-icons';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'category-management',
  templateUrl: './category-management.component.html',
  styleUrls: ['./category-management.component.css']
})
export class CategoryManagementComponent implements OnInit {
  categoriesList;
  selectedCategory;
  isCallInProgress = false;
  showPopup = false;
  rightIcon = faCaretRight;
  downIcon = faCaretDown;
  addCtgryForm;
  addSubCtgryForm;
  constructor(private categoryService: CategoryService, private toasterService: ToastrNotificationService,
              private ngbService: NgbModal, private modalService: ModalService) { }

  ngOnInit(): void {
    this.addCtgryForm = new FormGroup({
      categoryName: new FormControl('', Validators.required)
    });
    this.addSubCtgryForm = new FormGroup({
      categoryId: new FormControl('', Validators.required),
      subCategoryName: new FormControl('', Validators.required)
    });
    this.loadCategoryList();
  }

  loadCategoryList(idOfCategoryToExpand?: any) {
    this.categoryService.getAllCategories().subscribe(resp => {

      this.categoriesList = resp;
      if (idOfCategoryToExpand) {
        this.categoriesList.forEach(element => {
          if (element.categoryId === idOfCategoryToExpand) {
            element.showSubCategory = true;
          }
        });
      }
    }, (error) => {
      this.categoriesList = [];
      this.toasterService.showError('Error fetching category details');
    });
  }

  toggleSubCategoryCheckbox(subCategory, category) {
    if (!subCategory.activeFlag && !category.activeFlag) {
      this.toasterService.showError('Cannot activate sub category since Parent category is inactive');
      this.loadCategoryList(category.categoryId);
    } else {
      this.isCallInProgress = true;
      this.categoryService.updateSubCategoryStatus(subCategory.subCategoryId, !subCategory.activeFlag)
      .subscribe((resp) => {
        this.toasterService.showSuccess('Category status updated successfully');
      }, (error) => {
        this.toasterService.showError('Status updation failed');
      }).add(() => {
        this.isCallInProgress = false;
        this.loadCategoryList(category.categoryId);
      });
      }
  }

  callToggleService() {
    this.isCallInProgress = true;
    this.categoryService.updateCategoryStatus(this.selectedCategory.categoryId, !this.selectedCategory.activeFlag)
    .subscribe((resp) => {
      this.toasterService.showSuccess('Category status updated successfully');
    }, (error) => {
      this.toasterService.showError('Status updation failed');
    }).add(() => {
      this.isCallInProgress = false;
      this.loadCategoryList();
    });
  }

  open(content) {
    if (this.selectedCategory.subCategory && this.selectedCategory.subCategory.length > 0 && this.selectedCategory.activeFlag){
      this.ngbService.open(content, {ariaLabelledBy: 'modal-basic-title', centered: true})
      .result
      .then((result) => {
        if (result === 'OK') {
          this.callToggleService();
        } else {
          this.loadCategoryList();
        }
      });
  } else {
      this.callToggleService();
    }
  }

  openAddCtgry(content) {
    this.ngbService.open(content, {ariaLabelledBy: 'modal-basic-title', centered: true})
    .result
    .then((result) => {
      if (result === 'OK') {
        this.categoryService.addCategory(this.addCtgryForm.value).subscribe((resp) => {
          this.toasterService.showSuccess('Category added successfully');
          this.loadCategoryList();
        }, (error) => {
          this.toasterService.showError('Something went wrong');
        }).add(() => {
          this.addCtgryForm.reset();
        });
      } else {
        this.addCtgryForm.reset();
      }
    });
  }

  openAddSubCtgry(content) {
    this.ngbService.open(content, {ariaLabelledBy: 'modal-basic-title', centered: true})
    .result
    .then((result) => {
      if (result === 'OK') {
        this.categoryService.addSubCategory(this.addSubCtgryForm.value).subscribe((resp) => {
          this.toasterService.showSuccess('Sub category added successfully');
          this.loadCategoryList(this.addSubCtgryForm.controls.categoryId.value);
        }, (error) => {
          this.toasterService.showError('Something went wrong');
        }).add(() => {
          this.addSubCtgryForm.reset();
        });
      } else {
        this.addSubCtgryForm.reset();
      }
    });
  }

  toggleCategory(category){
    if (category.showSubCategory) {
      category.showSubCategory = false;
    } else {
      category.showSubCategory = true;
    }
  }

  openModal(id: string){
    this.modalService.open(id);
  }
}
