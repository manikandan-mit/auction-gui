import { Component, OnInit, Input } from '@angular/core';
import { ProductService } from 'src/app/services/products.service';
import { ToastrNotificationService } from 'src/app/services/toastr-notification.service';
import { ProductFilter } from 'src/app/models/product.filter';
import { ActivatedRoute } from '@angular/router';
import { ArtistService } from 'src/app/services/artist.service';
import { CategoryService } from 'src/app/services/category.service';
import { trigger, state, style, transition, animate } from '@angular/animations';

@Component({
  selector: 'products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.css'],
  animations: [
    trigger('openclose', [
      state('open', style({
        'margin-left': '0px'
      })),
      state('close', style({
        'margin-left': '-1000px'
      })),
      transition('open => close', [animate('.5s')]),
      transition('close => open', [animate('.5s')])
    ])
  ]
})
export class ProductsListComponent implements OnInit {
  productList: any[];
  productRows: any[] = [];
  artistList = [];
  subCategoryList = [];
  pageNumber: number;
  totalNumberOfElements: number;
  elementsPerPage: number;
  @Input() showPagination = true;
  showCriteria = false;
  showPopper = false;

  constructor(private productService: ProductService,
              private artistService: ArtistService,
              private categoryService: CategoryService,
              private activeRoute: ActivatedRoute,
              private toasterService: ToastrNotificationService) { }

  ngOnInit(): void {
    this.activeRoute.queryParams.subscribe(params => {
      this.loadProducts();
      this.artistService.listAllActiveArtist().subscribe((resp: any) => {
        this.artistList = resp;
        const a: string = this.productService.getProductFilter().ArtistId;
        const art: string[] = a ? a.split(',') : [];
        if (art.length > 0) {
          this.artistList.forEach(artist => {
            artist.selected = art.includes(artist.artistId.toString());
          });
        }
      }, (error) => {
        this.artistList = [];
      });
      this.categoryService.listSubCategory().subscribe((resp: any) => {
        this.subCategoryList = resp;
        const c: string = this.productService.getProductFilter().CategoryId;
        const cat: string[] = c ? c.split(',') : [];
        if (cat.length > 0) {
          this.subCategoryList.forEach(sub => {
            sub.selected = cat.includes(sub.subCategoryId.toString());
          });
        }
      }, (error) => {
        this.artistList = [];
      });
    });
  }

  loadProducts() {
    this.productService.getAllProducts(false).subscribe((resp) => {
      this.productList = resp.products;
      this.pageNumber = resp.pageNumber;
      this.totalNumberOfElements = resp.totalNumberOfElements;
      this.elementsPerPage = resp.productsPerPage;
      this.initialisePage();
    }, (error) => {
      this.toasterService.showError('Error retriving products. Please try again later');
    });
  }

  initialisePage() {
    this.productRows = [];
    const rowCount = Math.ceil(this.productList.length / 4);
    for (let index = 0; index < rowCount; index++) {
      this.productRows.push(this.productList.splice(0, 4));
    }
  }

  pageChange(event) {
    let filter: ProductFilter = this.productService.getProductFilter();
    if (filter) {
      filter.PageNumber = event;
    } else {
      filter = new ProductFilter(null, null, event, null);
    }
    this.productService.setProductFilter(filter);
    this.loadProducts();
  }

  filterProducts() {
    this.showPopper = false;
    this.callFilterProducts();
  }

  clearFilter() {
    this.artistList.forEach(artist => {
      artist.selected = false;
    });

    this.subCategoryList.forEach(sub => {
      sub.selected = false;
    });
    this.callFilterProducts();
  }

  callFilterProducts() {
    const selectedArtist = this.artistList.filter(artist => artist.selected).map(artist => artist.artistId);
    const selectedCategories = this.subCategoryList.filter(sub => sub.selected).map(sub => sub.subCategoryId);
    let artList = null;
    let catList = null;
    if (selectedArtist.length > 0) {
      artList = selectedArtist.join(',');
    }
    if (selectedCategories.length > 0) {
      catList = selectedCategories.join(',');
    }

    this.productService.setProductFilter(new ProductFilter(catList, artList, 1, this.productService.NUMBER_OF_ELEMENTS_PER_PAGE));
    this.loadProducts();
  }
}
