import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators, AbstractControl, ValidationErrors, AsyncValidatorFn } from '@angular/forms';
import { SecurityService } from 'src/app/services/security.service';
import { ToastrNotificationService } from 'src/app/services/toastr-notification.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  signupForm;
  @Output() close = new EventEmitter();
  constructor(private securityService: SecurityService, private _toastrNotificationService: ToastrNotificationService) { }

  ngOnInit(): void {
    this.signupForm = new FormGroup({
      username: new FormControl('', [Validators.required, Validators.minLength(5)], this.validateUsername()),
      password: new FormControl('', [Validators.required, Validators.minLength(8)]),
      confirmPassword: new FormControl(''),
      firstName: new FormControl('', Validators.required),
      lastName: new FormControl('', Validators.required),
      email: new FormControl('',
                             [Validators.required,
                              Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')],
                              this.validateEmail()),
      phone: new FormControl('', Validators.required, this.validatePhonenumber())
    }, this.passwordValidator);
  }

  passwordValidator(control: AbstractControl): {[key: string]: boolean} | null {
    const value = control.value;
    // if(!value.password && !value.confirmPassword)
    //   return null;

    if (value.password !== value.confirmPassword) {
      return {passwordMismatch: true};
    }

    return null;
  }

  validateUsername(): AsyncValidatorFn {
    return (control: AbstractControl): Observable<{ [key: string]: any } | null> => {
      return this.securityService.verifyIfUsernameExists(control.value)
        .pipe(
          map(res => {
            if (res) {
              return { usernameExists: true};
            } else {
              return null;
            }
          })
        );
    };
  }

  validateEmail(): AsyncValidatorFn {
    return (control: AbstractControl): Observable<{ [key: string]: any } | null> => {
      return this.securityService.verifyIfEmailExists(control.value)
        .pipe(
          map(res => {
            if (res) {
              return { emailExists: true};
            } else {
              return null;
            }
          })
        );
    };
  }

  validatePhonenumber(): AsyncValidatorFn {
    return (control: AbstractControl): Observable<{ [key: string]: any } | null> => {
      return this.securityService.verifyIfPhonenumberExists(control.value)
        .pipe(
          map(res => {
            if (res) {
              return { phoneExists: true};
            } else {
              return null;
            }
          })
        );
    };
  }

  submitForm() {
    this.securityService.signupUser(this.signupForm.value).subscribe((response) => {
      this._toastrNotificationService.showSuccess('User created successfully');
      this.closePopup();
    }, (error) => {
      this._toastrNotificationService.showError('User creation failed');
    });
  }

  closePopup(){
    this.signupForm.reset();
    this.close.emit();
  }

}
