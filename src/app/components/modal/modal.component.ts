import { Component, OnInit, OnDestroy, ViewEncapsulation, Input, ElementRef, EventEmitter, Output } from '@angular/core';
import { ModalService } from 'src/app/services/modal.service';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ModalComponent implements OnInit, OnDestroy {

  @Input() id;
  @Input() closeOnBgClick = true;
  @Output() modalClose = new EventEmitter();
  private element: any;

  constructor(private modalService: ModalService, private el: ElementRef) {
    this.element = el.nativeElement;
   }

  ngOnDestroy(): void {
    this.modalService.remove(this.id);
    this.element.remove();
  }

  ngOnInit(): void {
    if (!this.id) {
      console.error('modal must have an id');
      return;
    }

    document.body.appendChild(this.element);
    if (this.closeOnBgClick) {
      this.element.addEventListener('click', el => {
        if (el.target.className === 'modal-container') {
          this.close();
        }
      });
    }
    this.modalService.add(this);
  }

  open(): void {
    this.element.style.display = 'block';
    document.body.classList.add('modal-open');
  }

  close(): void {
    this.modalClose.emit();
    this.element.style.display = 'none';
    document.body.classList.remove('modal-open');
  }

}
