import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrNotificationService } from 'src/app/services/toastr-notification.service';
import { ProductService } from 'src/app/services/products.service';
import { faEdit, faPlusCircle, faTimes } from '@fortawesome/free-solid-svg-icons';
import { UserService } from 'src/app/services/user.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { textEditorConfig } from 'src/app/models/text-editor.config';
import { FormGroup, FormBuilder, FormArray, Validators } from '@angular/forms';
import { Location } from '@angular/common';
@Component({
  selector: 'product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css']
})
export class ProductDetailsComponent implements OnInit {
  productId;
  productDetails;
  editIcon = faEdit;
  isAdmin: boolean;
  productNameEditable;
  productDescEditable;
  estimateMinEditable;
  estimateMaxEditable;
  editorConfig: AngularEditorConfig = textEditorConfig;
  addInfoForm: FormGroup;
  noOfInfoRows;
  addIcon = faPlusCircle;
  removeIcon = faTimes;
  constructor(private route: ActivatedRoute, public router: Router,
              public formBuilder: FormBuilder,
              public location: Location,
              public productService: ProductService,
              private userService: UserService,
              private modalService: NgbModal,
              private toasterService: ToastrNotificationService) { }

    ngOnInit(): void {
      this.route.params.subscribe(params => {
        this.productId = params.id;
      });
      this.isAdmin = this.userService.isvalidAdmin();
      this.productService.getProductInfo(this.productId).subscribe((resp) => {
        this.productDetails = resp;
        this.productNameEditable = this.productDetails.productName;
        this.productDescEditable = this.productDetails.productDescription;
        this.estimateMinEditable = this.productDetails.estimatedMin;
        this.estimateMaxEditable = this.productDetails.estimatedMax;
        this.addInfoForm = this.formBuilder.group({
          additionalInfo: new FormArray([])
        });
        this.initializeAddInfoForm();
      }, (error) => {
        this.toasterService.showError('Artist information not found');
        this.router.navigate(['/products']);
      });
    }

    get f() { return this.addInfoForm.controls; }
    get a() {return this.f.additionalInfo as FormArray; }

    initializeAddInfoForm() {
      const info: any[] = this.getAdditionalInfo(this.productDetails.additionalInfo);
      this.noOfInfoRows = info.length;
      for (let i= 0; i < info.length; i++) {
        this.a.push(this.formBuilder.group({
          key: [info[i].key, Validators.required],
          value: [info[i].value, Validators.required]
        }));
      }
    }

    getAdditionalInfo(additionalInfoString: string): any[] {
      const temp: string[] = additionalInfoString.split('|');
      const infoList: any[] = [];
      temp.forEach(t => {
        infoList.push({
          key: t.split('=')[0],
          value: t.split('=')[1]
        });
      });
      return infoList;
    }

    editName(content) {
      this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
        this.callEditService('name', this.productNameEditable);
      }, (reason) => {

      });
    }

    editDesc(content) {
      this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
        this.callEditService('desc', this.productDescEditable);
      }, (reason) => {

      });
    }

    editEstimate(content) {
      this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
        this.callEditService('estimate', this.estimateMinEditable + '-' + this.estimateMaxEditable);
      }, (reason) => {

      });
    }

    editAddInfo(content) {
      this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
        this.callEditService('additionalInfo', this.a.value.map(info => info.key + '=' + info.value).join('|'));
      }, (reason) => {

      });
    }

    callEditService(field, value) {
      this.productService.updateProductInfo(field, value, this.productDetails.productId).subscribe((resp) => {
        this.toasterService.showSuccess('Product information updated!');
        window.location.reload();
      }, (error) => {
        this.toasterService.showError('Something went wrong.');
      });
    }

    addNewAddInfoRow() {
      this.noOfInfoRows = this.noOfInfoRows + 1;
      this.a.push(this.formBuilder.group({
        key: ['', Validators.required],
        value: ['', Validators.required]
      }));
    }

    removeRow(i) {
      this.a.removeAt(i);
    }

    routeToProducts() {
      this.location.back();
    }

}
