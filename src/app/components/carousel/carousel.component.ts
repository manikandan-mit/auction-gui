import { Component, OnInit } from '@angular/core';
import { BannerManagementService } from 'src/app/services/banner-management.service';

@Component({
  selector: 'carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.css']
})
export class CarouselComponent implements OnInit {

  bannerData;
  constructor(private _bannerManagementService: BannerManagementService) { }

  ngOnInit(): void {
    this._bannerManagementService.getBannerUrls().subscribe((resp) => {
      this.bannerData = resp;
    }, (error) => {
      this.bannerData = [];
    });
  }

}
