import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProductService } from 'src/app/services/products.service';
import { ProductFilter } from 'src/app/models/product.filter';

@Component({
  selector: 'latest-item',
  templateUrl: './latest-item.component.html',
  styleUrls: ['./latest-item.component.css']
})
export class LatestItemComponent implements OnInit {

  constructor(private router: Router, private productService: ProductService) { }

  ngOnInit(): void {
  }

  routeToProducts() {
    this.productService.setProductFilter(new ProductFilter(null, null, null, this.productService.NUMBER_OF_ELEMENTS_PER_PAGE));
    this.router.navigate(['/products']);
  }
}
