import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LatestItemComponent } from './latest-item.component';

describe('LatestItemComponent', () => {
  let component: LatestItemComponent;
  let fixture: ComponentFixture<LatestItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LatestItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LatestItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
