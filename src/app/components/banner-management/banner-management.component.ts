import { Component, OnInit } from '@angular/core';
import { Lightbox, LightboxConfig } from 'ngx-lightbox';
import { ToastrNotificationService } from 'src/app/services/toastr-notification.service';
import { BannerManagementService } from 'src/app/services/banner-management.service';

@Component({
  selector: 'banner-management',
  templateUrl: './banner-management.component.html',
  styleUrls: ['./banner-management.component.css']
})
export class BannerManagementComponent implements OnInit {

  allowedFileExtensions = ['.jpg', '.jpeg', '.png'];
  fileUploadError = null;
  fileToUpload: File = null;
  bannerList;
  showFileUploadInput = false;
  fileUploadInProgress = false;
  private _album = [];
  constructor(private _bannerManagementService: BannerManagementService, private _lightbox: Lightbox,
              private _lightboxConfig: LightboxConfig, private _toastrNotificationService: ToastrNotificationService) {
    this._lightboxConfig.centerVertically = true;
    this._lightboxConfig.wrapAround = true;
  }

  ngOnInit(): void {
    this.getAllBanners();
  }

  getAllBanners(){
    this._bannerManagementService.getBanners().subscribe((resp) => {
      this.bannerList = resp;
      this.bannerList.forEach(banner => {
        const album = {
          src: banner.url
        };
        this._album.push(album);
      });
    });
  }

  openImage(index: number): void {
    this._lightbox.open(this._album, index);
  }

  close(){
    this._lightbox.close();
  }

  deleteBanner(banner){
    banner.showSpinner = true;
    this._bannerManagementService.deleteBanner(banner.id, banner.publicId).subscribe((resp) => {
      this._toastrNotificationService.showError('Banner deleted successfully');
      this.getAllBanners();
    });
  }

  handleFileInput(files: FileList) {
    this.fileUploadError = null;
    const file = files.item(0);
    const fileExt = file.name.substr(file.name.lastIndexOf('.'));

    if (!this.allowedFileExtensions.includes(fileExt)){
      this.fileUploadError = 'Only .jpg, .jpeg, .png files are allowed';
      return;
    }
    if (file.size > 5242880) {
      this.fileUploadError = 'File size must be below 5MB';
      return;
    }

    this.fileToUpload = files.item(0);
}

  uploadFileToActivity() {
    this.fileUploadInProgress = true;
    this._bannerManagementService.uploadFile(this.fileToUpload).subscribe(() => {
        this.fileUploadInProgress = false;
        this.showFileUploadInput = false;
        this.fileUploadError = null;
        this.fileToUpload = null;
        this._toastrNotificationService.showSuccess('Banner uploaded successfully');
        this.getAllBanners();
      }, error => {
        this.fileUploadInProgress = false;
        this._toastrNotificationService.showError('Banner uploaded failed');
      });
  }
}
