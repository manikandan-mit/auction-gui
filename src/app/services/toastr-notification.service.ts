import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class ToastrNotificationService {

  constructor(private _toastrService: ToastrService) { }

  showSuccess(message: string){
    this._toastrService.success(message, null, {closeButton: true});
  }

  showError(message: string){
    this._toastrService.error(message, null, {closeButton: true});
  }

  showInfo(message: string){
    this._toastrService.info(message, null, {closeButton: true});
  }

  showWarning(message: string){
    this._toastrService.warning(message, null, {closeButton: true});
  }
}
