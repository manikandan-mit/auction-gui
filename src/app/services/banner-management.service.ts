import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {environment} from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BannerManagementService {

  constructor(private _httpClient: HttpClient) { }

  getBannerUrls() {
    return this._httpClient.get(environment.context + '/banners/public/get');
  }

  getBanners() {
    return this._httpClient.get(environment.context + '/banners/get');
  }

  deleteBanner(bannerId: string, publicId: string) {
    return this._httpClient.delete(environment.context.concat('/banners/').concat(bannerId).concat('/').concat(publicId));
  }

  uploadFile(fileToUpload: File){
    const formData = new FormData();
    formData.append('file', fileToUpload, fileToUpload.name);
    return this._httpClient.post(environment.context + '/banners/upload', formData);
  }
}
