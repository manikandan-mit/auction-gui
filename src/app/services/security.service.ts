import { Injectable, ɵSWITCH_COMPILE_INJECTABLE__POST_R3__ } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {environment} from 'src/environments/environment';
import { timer } from 'rxjs';
import { switchMap  } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SecurityService {

  constructor(private _httpClient: HttpClient) { }

  verifyIfUsernameExists(username: string) {
    return timer(1000)
            .pipe(
              switchMap(() => {
                return this._httpClient.get(environment.context + '/security/username/' + username);
              })
            );
  }

  verifyIfEmailExists(email: string) {
    return timer(1000)
            .pipe(
              switchMap(() => {
                return this._httpClient.get(environment.context + '/security/email/' + email);
              })
            );
  }

  verifyIfPhonenumberExists(phone: string) {
    return timer(1000)
            .pipe(
              switchMap(() => {
                return this._httpClient.get(environment.context + '/security/phone/' + phone);
              })
            );
  }

  signupUser(signupRequest: object){
    return this._httpClient.post(environment.context + '/security/signup', signupRequest);
  }

  loginUser(login: object) {
    return this._httpClient.post(environment.context + '/security/login', login);
  }

  getToken(){
    return localStorage.getItem('token');
  }

  setTokenInLocal(token: string): void{
    localStorage.setItem('token', token);
  }

  removeTokenFromLocal(){
    localStorage.removeItem('token');
  }
}
