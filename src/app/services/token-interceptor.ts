import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SecurityService } from './security.service';
import { tap } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

    constructor(private securityService: SecurityService, private router: Router) {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const url = request.url;
        if (!url.includes('/security') && !url.includes('/public')) {
            request = request.clone({
                setHeaders: {
                    token: 'Bearer ' + this.securityService.getToken()
                }
            });
        }

        return next.handle(request).pipe(tap(() => {}, (error: any) => {
            if (error instanceof HttpErrorResponse) {
                if (error.status !== 401) {
                    return;
                }
                localStorage.removeItem('token');
                this.router.navigate(['/home']);
            }
        }));
    }

}
