import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { ProductFilter } from '../models/product.filter';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  private filter: ProductFilter;
  public NUMBER_OF_ELEMENTS_PER_PAGE = 4;
  constructor(private _httpClient: HttpClient) { }

  getProductFilter(): ProductFilter {
    return this.filter;
  }

  setProductFilter(filter: ProductFilter) {
    this.filter = filter;
  }

  getAllProducts(getListOnly: boolean, status?: number): Observable<any> {
    let url: string = environment.context + '/products/public/get';
    if (!this.filter) {
      this.filter = new ProductFilter(null, null, 1, this.NUMBER_OF_ELEMENTS_PER_PAGE);
    }

    url = url + '?';
    if ( this.filter.ArtistId) {
      url = url + 'artistId=' + this.filter.ArtistId + '&';
    }
    if (this.filter.CategoryId) {
      url = url + 'categoryId=' + this.filter.CategoryId + '&';
    }
    if (this.filter.PageNumber) {
      url = url + 'pageNo=' + this.filter.PageNumber + '&';
    }
    if (this.filter.PageSize) {
      url = url + 'pageSize=' + this.filter.PageSize + '&';
    }

    if (status) {
      url = url + 'status=' + status + '&';
    }
    url = url + 'getListOnly=' + getListOnly;

    return this._httpClient.get(url);
  }

  getProductInfo(productId: any): Observable<any> {
    return this._httpClient.get(environment.context + '/products/public/get/' + productId);
  }

  updateProductStatus(productIds: number[], status: number) {
    return this._httpClient.get(environment.context + '/products/update/' + productIds.join(',') + '/' + status);
  }

  addNewProduct(productObj) {
    return this._httpClient.post(environment.context + '/products/add', productObj);
  }

  updateProductInfo(fieldname: string, fieldValue: string, productId: number) {
    const url = environment.context + '/products/update?fieldName=' + fieldname + '&fieldValue=' + fieldValue + '&productId=' + productId;
    return this._httpClient.get(encodeURI(url));
  }
}
