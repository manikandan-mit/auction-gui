import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {environment} from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor(private _httpClient: HttpClient) { }

  getAllCategories(){
    return this._httpClient.get(environment.context + '/categories/get');
  }

  updateCategoryStatus(userId: string, activeStatus: boolean) {
    const data = {
      id: userId,
      activeStatus
    };
    return this._httpClient.patch(environment.context + '/categories/update/category', data);
  }

  updateSubCategoryStatus(userId: string, activeStatus: boolean) {
    const data = {
      id: userId,
      activeStatus
    };
    return this._httpClient.patch(environment.context + '/categories/update/subcategory', data);
  }

  addCategory(category) {
    return this._httpClient.post(environment.context + '/categories/add', category);
  }

  addSubCategory(category) {
    return this._httpClient.post(environment.context + '/categories/add/subcategory', category);
  }

listSubCategory() {
  return this._httpClient.get(environment.context + '/categories/public/subcategory/list');
}

}
