import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {environment} from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private userDetails: any;
  constructor(private _httpClient: HttpClient) { }

  initialieApp(): Promise<any>{
    return new Promise((resolve, reject) => {
      if (localStorage.getItem('token') != null) {
        this.loadUserDetails().subscribe((resp) => {
          this.setUserDetails(resp);
          resolve(true);
        }, (error) => {
          this.setUserDetails(null);
          localStorage.removeItem('token');
          resolve(true);
        });
      } else {
        this.setUserDetails(null);
        resolve(true);
      }
    });
  }

  loadUserDetails() {
    return this._httpClient.get(environment.context + '/user/me');
  }

  getAllusers() {
    return this._httpClient.get(environment.context + '/user/get');
  }

  updateUserStatus(userId: string, activeStatus: boolean) {
    const data = {
      id: userId,
      activeStatus
    };
    return this._httpClient.patch(environment.context + '/user/update/status', data);
  }

  setUserDetails(userDetails: any): void {
    this.userDetails = userDetails;
  }

  getUserDetails(): any {
    return this.userDetails;
  }

  isValidUser(): boolean {
    return this.userDetails != null;
  }

  isvalidAdmin() {
    if (this.userDetails != null) {
     const roles: string[] = this.userDetails.roles;
     if (roles.includes('ADMIN')) {
       return true;
     }
    }
    return false;
  }
}
