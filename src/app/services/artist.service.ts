import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {environment} from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ArtistService {

  constructor(private _httpClient: HttpClient) { }

  getArtistInfo(artistId: string, populateDefaultImg: boolean) {
    return this._httpClient.get(environment.context + '/artist/public/get/' + artistId + '?populateDefaultProfile=' + populateDefaultImg);
  }

  getAllArtist() {
    return this._httpClient.get(environment.context + '/artist/get');
  }

  updateArtistStatus(artistId: string, activeStatus: boolean) {
    const data = {
      id: artistId,
      activeStatus
    };
    return this._httpClient.patch(environment.context + '/artist/update', data);
  }

  saveArtistWithImage(artistObj, deleteId) {
    let url = environment.context + '/artist/save';
    if (deleteId) {
      url = url + '?deleteId=' + deleteId;
    }
    return this._httpClient.post(encodeURI(url), artistObj);
  }

  saveArtistInfo(artistObj, deleteId) {
    let url = environment.context + '/artist/info/save';
    if (deleteId) {
      url = url + '?deleteId=' + deleteId;
    }
    return this._httpClient.post(encodeURI(url), artistObj);
  }

  listAllActiveArtist() {
    return this._httpClient.get(environment.context + '/artist/public/list');
  }
}
