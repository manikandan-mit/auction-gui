import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {environment} from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MenuBarService {

  constructor(private _httpClient: HttpClient) { }

  getCategoriesMenu(){
    return this._httpClient.get(environment.context + '/categories/public/get');
  }
}
