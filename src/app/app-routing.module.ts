import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { BannerManagementComponent } from './components/banner-management/banner-management.component';
import { AdminGuard } from './guards/admin.guard';
import { UserManagementComponent } from './components/user-management/user-management.component';
import { CategoryManagementComponent } from './components/category-management/category-management.component';
import { ArtistComponent } from './components/artist/artist.component';
import { ArtistManagementComponent } from './components/artist-management/artist-management.component';
import { ProductsListComponent } from './components/products-list/products-list.component';
import { ProductDetailsComponent } from './components/product-details/product-details.component';
import { AboutUsComponent } from './components/about-us/about-us.component';
import { AddArtistComponent } from './components/add-artist/add-artist.component';
import { ProductManagementComponent } from './components/product-management/product-management.component';
import { AddProductComponent } from './components/add-product/add-product.component';
import { ContactUsComponent } from './components/contact-us/contact-us.component';
import { FaqComponent } from './components/faq/faq.component';
import { TermsAndConditionsComponent } from './components/terms-and-conditions/terms-and-conditions.component';


const routes: Routes = [
  {path: '', redirectTo: 'home', pathMatch: 'full'},
  {path: 'home', component: HomeComponent},
  {path: 'artist/:id', component: ArtistComponent},
  {path: 'products', component: ProductsListComponent},
  {path: 'products/:id', component: ProductDetailsComponent},
  {path: 'admin/banner', component: BannerManagementComponent, canActivate: [AdminGuard]},
  {path: 'admin/users', component: UserManagementComponent, canActivate: [AdminGuard]},
  {path: 'admin/categories', component: CategoryManagementComponent, canActivate: [AdminGuard]},
  {path: 'admin/artist/add', component: AddArtistComponent, canActivate: [AdminGuard]},
  {path: 'admin/artist/add/:id', component: AddArtistComponent, canActivate: [AdminGuard]},
  {path: 'admin/artist', component: ArtistManagementComponent, canActivate: [AdminGuard]},
  {path: 'admin/products', component: ProductManagementComponent, canActivate: [AdminGuard]},
  {path: 'admin/products/add', component: AddProductComponent, canActivate: [AdminGuard]},
  {path: 'about', component: AboutUsComponent},
  {path: 'contact', component: ContactUsComponent},
  {path: 'faq', component: FaqComponent},
  {path: 'tandc', component: TermsAndConditionsComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
