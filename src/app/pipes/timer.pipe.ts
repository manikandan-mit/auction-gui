import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'timer'})
export class TimerPipe implements PipeTransform{
    transform(value: any, ...args: any[]) {
        const hrs = Math.trunc(value / (60 * 60));
        const mins = Math.trunc(value / (60)) - (hrs * 60);
        const secs = Math.trunc(value - (mins * 60) - (hrs * 60 * 60));
        return hrs + ':' + mins + ':' + secs;
    }
}
