export class ProductFilter{
    constructor(
        public CategoryId: string,
        public ArtistId: string,
        public PageNumber: number,
        public PageSize: number
    ){ }
}
