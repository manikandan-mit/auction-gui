import { BrowserModule } from '@angular/platform-browser';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { LightboxModule } from 'ngx-lightbox';
import { ToastrModule } from 'ngx-toastr';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MenuBarComponent } from './components/menu-bar/menu-bar.component';
import { HomeComponent } from './components/home/home.component';
import { CarouselComponent } from './components/carousel/carousel.component';
import { LatestItemComponent } from './components/latest-item/latest-item.component';
import { FooterComponent } from './components/footer/footer.component';
import { ModalComponent } from './components/modal/modal.component';
import { LoginComponent } from './components/login/login.component';
import { SignupComponent } from './components/signup/signup.component';
import { UserService } from './services/user.service';
import { TokenInterceptor } from './services/token-interceptor';
import { BannerManagementComponent } from './components/banner-management/banner-management.component';
import { UserGuard } from './guards/user.guard';
import { AdminGuard } from './guards/admin.guard';
import { UserManagementComponent } from './components/user-management/user-management.component';
import { UiSwitchModule } from 'ngx-toggle-switch';
import { CategoryManagementComponent } from './components/category-management/category-management.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ArtistComponent } from './components/artist/artist.component';
import { ArtistManagementComponent } from './components/artist-management/artist-management.component';
import { ProductsListComponent } from './components/products-list/products-list.component';
import { ProductInfoComponent } from './components/product-info/product-info.component';
import { ProductDetailsComponent } from './components/product-details/product-details.component';
import { TimerPipe } from './pipes/timer.pipe';
import { AboutUsComponent } from './components/about-us/about-us.component';
import { AddArtistComponent } from './components/add-artist/add-artist.component';
import { PaginationComponent } from './components/pagination/pagination.component';
import { ProductManagementComponent } from './components/product-management/product-management.component';
import { AddProductComponent } from './components/add-product/add-product.component';
import { ContactUsComponent } from './components/contact-us/contact-us.component';
import { FaqComponent } from './components/faq/faq.component';
import { TermsAndConditionsComponent } from './components/terms-and-conditions/terms-and-conditions.component';
import { PopperComponent } from './components/popper/popper.component';

export function init_app(userService: UserService) {
  return () => userService.initialieApp();
}

@NgModule({
  declarations: [
    AppComponent,
    MenuBarComponent,
    HomeComponent,
    CarouselComponent,
    LatestItemComponent,
    FooterComponent,
    ModalComponent,
    LoginComponent,
    SignupComponent,
    BannerManagementComponent,
    UserManagementComponent,
    CategoryManagementComponent,
    ArtistComponent,
    ArtistManagementComponent,
    ProductsListComponent,
    ProductInfoComponent,
    ProductDetailsComponent,
    TimerPipe,
    AboutUsComponent,
    AddArtistComponent,
    PaginationComponent,
    ProductManagementComponent,
    AddProductComponent,
    ContactUsComponent,
    FaqComponent,
    TermsAndConditionsComponent,
    PopperComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    LightboxModule,
    AngularEditorModule,
    ToastrModule.forRoot(),
    FontAwesomeModule,
    ReactiveFormsModule,
    FormsModule,
    UiSwitchModule,
    NgbModule
  ],
  providers: [
    {provide: APP_INITIALIZER, useFactory: init_app, deps: [UserService], multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true},
    UserGuard, AdminGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
